from django.contrib import admin
from .models import Banks, Bank_Customers, Accounts, AccountType

# Register your models here.

admin.site.register(Banks)
admin.site.register(Bank_Customers)
admin.site.register(Accounts)
admin.site.register(AccountType)
