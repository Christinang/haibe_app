from rest_framework import serializers
from rest_framework.serializers import EmailField, ValidationError, CharField
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from django.shortcuts import render, HttpResponse
from .models import Banks, Bank_Customers, CustomerUser, AccountType, Accounts

class BanksListSerializer(serializers.ModelSerializer):
	result = CharField(allow_blank=True, read_only=True)
	class Meta:
		model = Banks
		fields = ['id', 'Bank_name', 'result',]

class BanksAddSerializer(serializers.ModelSerializer):
	result = CharField(allow_blank=True, read_only=True)
	class Meta:
		model = Banks
		fields = ['id', 'Bank_name', 'result',]

class BanksEditSerializer(serializers.ModelSerializer):
	class Meta:
		model = Banks
		fields = ['id', 'Bank_name',]

class BankCustomerListSerializer(serializers.ModelSerializer):
	result = CharField(allow_blank=True, read_only=True)
	Bank_id = BanksListSerializer()
	class Meta:
		model = Bank_Customers
		fields = ['id', 'Customer_name', 'Contact_info', 'Bank_id', 'father_fullname', 'mother_maiden_fullname', 'address', 'picture', 'valid_id1', 'valid_id2', 'result',]

class BankCustomerAddSerializer(serializers.ModelSerializer):
	result = CharField(allow_blank=True, read_only=True)
	# Bank_id = BanksListSerializer()
	class Meta:
		model = Bank_Customers
		fields = ['id', 'Customer_name', 'Contact_info', 'Bank_id', 'father_fullname', 'mother_maiden_fullname', 'address', 'picture', 'valid_id1', 'valid_id2', 'result',]

class BankCustomerEditSerializer(serializers.ModelSerializer):
	class Meta:
		model = Bank_Customers
		fields = ['id', 'Customer_name', 'Contact_info', 'Bank_id', 'father_fullname', 'mother_maiden_fullname', 'address', 'picture', 'valid_id1', 'valid_id2',]

class AccountTypeListSerializer(serializers.ModelSerializer):
	result = CharField(allow_blank=True, read_only=True)
	class Meta:
		model = AccountType
		fields = ['id', 'Account_type', 'result',]

class AccountTypeAddSerializer(serializers.ModelSerializer):
	result = CharField(allow_blank=True, read_only=True)
	class Meta:
		model = AccountType
		fields = ['id', 'Account_type', 'result',]

class AccountTypeEditSerializer(serializers.ModelSerializer):
	result = CharField(allow_blank=True, read_only=True)
	class Meta:
		model = AccountType
		fields = ['id', 'Account_type',]

class AccountListSerializer(serializers.ModelSerializer):
	result = CharField(allow_blank=True, read_only=True)
	Customer_id = BankCustomerListSerializer()
	Account_type_id = AccountTypeListSerializer()
	class Meta:
		model = Accounts
		fields = ['id', 'Customer_id', 'Account_type_id', 'Balance', 'result',]

class AccountAddSerializer(serializers.ModelSerializer):
	result = CharField(allow_blank=True, read_only=True)
	# Customer_id = BankCustomerListSerializer()
	# Account_type_id = AccountTypeListSerializer()
	class Meta:
		model = Accounts
		fields = ['id', 'Customer_id', 'Account_type_id', 'Balance', 'result',]

class AccountEditSerializer(serializers.ModelSerializer):
	result = CharField(allow_blank=True, read_only=True)
	class Meta:
		model = Accounts
		fields = ['id', 'Customer_id', 'Account_type_id', 'Balance',]

class CustomerUserSerializer(serializers.ModelSerializer):
	class Meta:
		model = CustomerUser
		fields = ['id', 'username', 'password',]

class CustomerUserAddSerializer(serializers.ModelSerializer):
	result = CharField(allow_blank=True, read_only=True)
	class Meta:
		model = CustomerUser
		fields = ['id', 'username', 'password', 'Customer_id', 'result',]

class CustomerUserEditSerializer(serializers.ModelSerializer):
	result = CharField(allow_blank=True, read_only=True)
	class Meta:
		model = CustomerUser
		fields = ['id', 'username', 'password',]

class CustomerUserLogInSerializer(serializers.ModelSerializer):
	result = CharField(allow_blank=True, read_only=True)
	class Meta:
		model = CustomerUser
		fields = ['id', 'username', 'password', 'Customer_id', 'result',]