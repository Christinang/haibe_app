from django.db import models
from django.conf import settings

# Create your models here.
class Banks(models.Model):
	Bank_name = models.CharField(max_length = 100, null = True)

	def __str__(self):
		return self.Bank_name

class Bank_Customers(models.Model):
	Customer_name = models.CharField(max_length = 100, null = True)
	Contact_info = models.CharField(max_length = 50, null = True)
	Bank_id = models.ForeignKey(Banks, on_delete = models.CASCADE, null = True)
	father_fullname = models.CharField(max_length = 100, null = True)
	mother_maiden_fullname = models.CharField(max_length = 100, null = True)
	address = models.CharField(max_length = 200, null = True)
	picture = models.CharField(max_length = 500, null = True)
	valid_id1 = models.CharField(max_length = 500, null = True)
	valid_id2 = models.CharField(max_length = 500, null = True)

	def __str__(self):
		return self.Customer_name

class CustomerUser(models.Model):
	Customer_id = models.OneToOneField(Bank_Customers, on_delete = models.CASCADE, null = True)
	username = models.CharField(max_length = 30, null = True)
	password = models.CharField(max_length = 50, null = True)

	def __str__(self):
		return self.username

class AccountType(models.Model):
	Account_type = models.CharField(max_length = 30, null = True)

class Accounts(models.Model):
	Customer_id = models.ForeignKey(Bank_Customers, on_delete = models.CASCADE, null = True)
	Account_type_id = models.ForeignKey(AccountType, on_delete = models.CASCADE, null = True)
	Balance = models.DecimalField(max_digits = 19, decimal_places=2, null = True)
