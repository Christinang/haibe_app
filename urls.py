from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^bank/$', views.banks.as_view()),
    url(r'^bank/(?P<id>\d+)$', views.banks.as_view()),
    url(r'^bank_customers/$', views.bank_customers.as_view()),
    url(r'^bank_customers/(?P<id>\d+)$', views.bank_customers.as_view()),
    url(r'^accountType/$', views.accountType.as_view()),
    url(r'^accountType/(?P<id>\d+)$', views.accountType.as_view()),
    url(r'^account/$', views.account.as_view()),
    url(r'^account/(?P<id>\d+)$', views.account.as_view()),
    url(r'^customer_user/$', views.customer_user.as_view()),
    url(r'^customer_user/(?P<id>\d+)$', views.customer_user.as_view()),
    url(r'^login/', views.login.as_view())
]