from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED, HTTP_302_FOUND, HTTP_303_SEE_OTHER, HTTP_400_BAD_REQUEST
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from rest_framework import parsers, renderers, generics, status
from .models import Banks, Bank_Customers, CustomerUser, AccountType, Accounts
from .serializers import BanksListSerializer, BanksAddSerializer, BanksEditSerializer, BankCustomerListSerializer, BankCustomerAddSerializer, BankCustomerEditSerializer, AccountTypeListSerializer, AccountTypeAddSerializer, AccountTypeEditSerializer, AccountListSerializer, AccountAddSerializer, AccountEditSerializer, CustomerUserSerializer, CustomerUserAddSerializer, CustomerUserEditSerializer, CustomerUserLogInSerializer
from django.http import HttpResponse
# Create your views here.

class banks(APIView):
	def get(self, request, format=None):
		if 'id' in request.GET:
			try:
				id = request.GET['id']
				banks = Banks.objects.get(pk=id)
				serializer = BanksListSerializer(banks)
				return Response(serializer.data, status=status.HTTP_302_FOUND)
			except Banks.DoesNotExist:
				result = "Banks Does Not Exist!"
				return(result)

		else:
			banks = Banks.objects.all()
			serializer = BanksListSerializer(banks, many = True)
			return Response(serializer.data, status=status.HTTP_303_SEE_OTHER)

	def post(self, request):
		serializer = BanksAddSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		else:
			result = "Bank Already Exist!"
			return Response(result)

	def put(self, request):
		id = request.GET['id']
		banks = Banks.objects.get(pk=id)
		serializer = BanksEditSerializer(banks, data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_200_OK)
		else:
			result = "No Changes"
			return Response(result)

class bank_customers(APIView):
	def get(self, request, format=None):
		if 'id' in request.GET:
			try:
				id = request.GET['id']
				banksCustomer = Bank_Customers.objects.get(pk=id)
				serializer = BankCustomerListSerializer(banksCustomer)
				return Response(serializer.data, status=status.HTTP_302_FOUND)
			except Bank_Customers.DoesNotExist:
				result = "Customer Does Not Exist!"
				return(result)

		else:
			banksCustomer = Bank_Customers.objects.all()
			serializer = BankCustomerListSerializer(banksCustomer, many = True)
			return Response(serializer.data, status=status.HTTP_303_SEE_OTHER)

	def post(self, request):
		serializer = BankCustomerAddSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		else:
			result = "Customer Already Exist!"
			return Response(result)

	def put(self, request):
		id = request.GET['id']
		banksCustomer = Bank_Customers.objects.get(pk=id)
		serializer = BankCustomerEditSerializer(banksCustomer, data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_200_OK)
		else:
			result = "No Changes"
			return Response(result)

class accountType(APIView):
	def get(self, request, format=None):
		if 'id' in request.GET:
			try:
				id = request.GET['id']
				accountype = AccountType.objects.get(pk=id)
				serializer = AccountTypeListSerializer(accountype)
				return Response(serializer.data, status=status.HTTP_302_FOUND)
			except AccountType.DoesNotExist:
				result = "Account Type Does Not Exist!"
				return(result)

		else:
			accountype = AccountType.objects.all()
			serializer = AccountTypeListSerializer(accountype, many = True)
			return Response(serializer.data, status=status.HTTP_303_SEE_OTHER)

	def post(self, request):
		serializer = AccountTypeAddSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		else:
			result = "Account Type Already Exist!"
			return Response(result)

	def put(self, request):
		id = request.GET['id']
		accountype = AccountType.objects.get(pk=id)
		serializer = AccountTypeEditSerializer(accountype, data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_200_OK)
		else:
			result = "No Changes"
			return Response(result)

class account(APIView):
	def get(self, request, format=None):
		if 'id' in request.GET:
			try:
				id = request.GET['id']
				acc = Accounts.objects.get(pk=id)
				serializer = AccountListSerializer(acc)
				return Response(serializer.data, status=status.HTTP_302_FOUND)
			except Accounts.DoesNotExist:
				result = "Account Does Not Exist!"
				return(result)

		else:
			acc = Accounts.objects.all()
			serializer = AccountListSerializer(acc, many = True)
			return Response(serializer.data, status=status.HTTP_303_SEE_OTHER)

	def post(self, request):
		serializer = AccountAddSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		else:
			result = "Account Already Exist!"
			return Response(result)

	def put(self, request):
		id = request.GET['id']
		acc = Accounts.objects.get(pk=id)
		serializer = AccountEditSerializer(acc, data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_200_OK)
		else:
			result = "No Changes"
			return Response(result)

class customer_user(APIView):
	def get(self, request, format=None):
		if 'id' in request.GET:
			try:
				id = request.GET['id']
				customer_user = CustomerUser.objects.get(pk=id)
				serializer = CustomerUserSerializer(customer_user)
				return Response(serializer.data, status=status.HTTP_302_FOUND)
			except CustomerUser.DoesNotExist:
				result = "User Does Not Exist!"
				return(result)

		else:
			customer_user = CustomerUser.objects.all()
			serializer = CustomerUserSerializer(customer_user, many = True)
			return Response(serializer.data, status=status.HTTP_303_SEE_OTHER)

	def post(self, request):
		serializer = CustomerUserAddSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		else:
			result = "User Already Exist!"
			return Response(result)

	def put(self, request):
		id = request.GET['id']
		customer_user = CustomerUser.objects.get(pk=id)
		serializer = CustomerUserEditSerializer(customer_user, data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_200_OK)
		else:
			result = "No Changes"
			return Response(result)

class login(APIView):
	def post(self, request, format=None):
		username = request.data['username']
		password = request.data['password']

		try:
			CustomerUser.objects.get(username=username, password=password)
		except CustomerUser.DoesNotExist:
			customer_user = CustomerUser()
			customer_user.result = "Error"
			serializer = CustomerUserLogInSerializer(customer_user)
			return Response(serializer.data)
		else:
			customer_user = CustomerUser.objects.get(username=username, password=password)
			customer_user.result = "Successful"
			serializer = CustomerUserLogInSerializer(customer_user)
			return Response(serializer.data)